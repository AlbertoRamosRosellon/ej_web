$(document).on("click", "#anterior", function () {
    var minimo = parseInt($(".enlace:first").text());
    if (minimo == 0) {
        console.log("has llegado al mínimo");
    } else {
        $(".enlace").each(function () {
            var valor = parseInt($(this).text());
            valor--;
            $(this).text(valor);
            $(this).attr("href", "pagina.html?p="+valor);
        })
    }
});

$(document).on("click", "#siguiente", function () {

    var maximo = parseInt($(".enlace:last").text());
    if (maximo == 20) {
        console.log("has llegado al máximo");
    } else {

        $(".enlace").each(function () {
            var valor = parseInt($(this).text());
            valor++;
            $(this).text(valor);
            $(this).attr("href", "pagina.html?p="+valor);
        })
    }
});
